import javax.persistence.Column;
import  javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="carro")
public class Carro {
		
	public Carro(String placa, String montadora, String modelo, int ano) {
		super();
		this.placa = placa;
		this.montadora = montadora;
		this.modelo = modelo;
		this.ano = ano;
	}
	@Id
	private String placa;
	@Column(name="marca")
	private String montadora;
	private String modelo;
	private int ano;
}
 