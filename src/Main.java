import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {
	public static void main(String[] args) {
		
		Carro carro = new Carro("AED8766", "VW", "Fusca", 1978);
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Carro");
		EntityManager manager = factory.createEntityManager();
		
		manager.getTransaction().begin();
		manager.persist(carro);
		manager.getTransaction().commit();
		
		manager.close();
		factory.close();
		
		factory.close();
	}
}
